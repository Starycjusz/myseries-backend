<?php

namespace App\Service\Exception;

use App\Service\Cache\CacheFactoryInterface;
use App\Service\Exception\Handler\DefaultExceptionHandler;
use App\Service\Exception\Handler\ExceptionHandlerInterface;
use Exception;
use HaydenPierce\ClassFinder\ClassFinder;
use Psr\Cache\InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use Throwable;

class ExceptionHandlerFactory implements ExceptionHandlerFactoryInterface
{
    /**
     * @param CacheFactoryInterface $cacheFactory
     * @param string                $logsDir
     * @param bool                  $isDebug
     */
    public function __construct(private CacheFactoryInterface $cacheFactory, private string $logsDir, private bool $isDebug)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getHandler(Throwable $throwable): ExceptionHandlerInterface
    {
        $map = $this->getHandlerMap();

        $handlerClass = $map[get_class($throwable)] ?? DefaultExceptionHandler::class;

        return $this->createHandler($handlerClass);
    }

    /**
     * @return array
     */
    private function getHandlerMap(): array
    {
        $cache = $this->cacheFactory->getTagAware('exception_handler_map');

        try {
            $cacheItem = $cache->getItem('exception_handler_map');
        } catch (InvalidArgumentException $e) {
            return $this->buildMap();
        }

        if (!$cacheItem->isHit()) {
            $cacheItem->set($this->buildMap());
            $cache->save($cacheItem);
        }

        return $cacheItem->get();
    }

    /**
     * @return array
     */
    private function buildMap(): array
    {
        try {
            $classes = ClassFinder::getClassesInNamespace(
                'App\Service\Exception\Handler',
                ClassFinder::RECURSIVE_MODE
            );
        } catch (Exception $e) {
            return [];
        }

        $loaders = [];

        foreach ($classes as $class) {
            if (ExceptionHandlerInterface::class === $class) {
                continue;
            }

            try {
                $reflect = new ReflectionClass($class);
            } catch (ReflectionException $e) {
                continue;
            }

            if ($reflect->implementsInterface(ExceptionHandlerInterface::class)) {
                $loaders[$class::getSupportedClass()] = $class;
            }
        }

        return $loaders;
    }

    /**
     * @param string $handlerClass
     *
     * @return ExceptionHandlerInterface
     */
    private function createHandler(string $handlerClass): ExceptionHandlerInterface
    {
        return new $handlerClass($this->logsDir, $this->isDebug);
    }
}
