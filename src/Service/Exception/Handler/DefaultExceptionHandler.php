<?php

namespace App\Service\Exception\Handler;

use ErrorException;
use DateTime;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Throwable;

class DefaultExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(private string $logsDir, private bool $isDebug)
    {
    }

    /**
     * {@inheritDoc}
     */
    public static function getSupportedClass(): string
    {
        return Throwable::class;
    }

    /**
     * {@inheritDoc}
     */
    public function getResponse(Throwable $throwable): JsonResponse
    {
        $errorInfo = [
            'message' => $throwable->getMessage(),
            'code'    => $throwable->getCode(),
            'file'    => $throwable->getFile(),
            'line'    => $throwable->getLine(),
            'trace'   => $throwable->getTrace(),
        ];

        if ($this->isDebug) {
            return new JsonResponse($errorInfo, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        $dt = new DateTime();
        $errorCode = md5($dt->getTimestamp().uniqid());

        $errorsDir = sprintf('%s/unhandled_errors', $this->logsDir);

        $fileName = $dt->format('Y-m-d H:i:s').' --- '.$errorCode;
        $fileDir = $errorsDir.'/'.$dt->format('Y-m-d').'/'.$fileName.'.html';

        try {
            $dump = (new HtmlDumper())->dump((new VarCloner())->cloneVar($throwable), true);
        } catch (ErrorException $e) {
            $errorCode = null;
        }

        if ($errorCode) {
            $fs = new Filesystem();
            try {
                $fs->dumpFile($fileDir, $dump ?? '');
            } catch (IOException $e) {
                $errorCode = null;
            }
        }

        return new JsonResponse(
            [
                'errors' => [
                    'messages' => [
                        [
                            'message' => 'Something went wrong. Please contact the support.',
                            'value'   => $errorCode,
                        ],
                    ],
                ],
            ],
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
