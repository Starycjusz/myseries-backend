<?php

namespace App\Service\Exception\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Throwable;

interface ExceptionHandlerInterface
{
    /**
     * @param string $logsDir
     * @param bool   $isDebug
     */
    public function __construct(string $logsDir, bool $isDebug);

    /**
     * @return string
     */
    public static function getSupportedClass(): string;

    /**
     * @param Throwable $throwable
     *
     * @return JsonResponse
     */
    public function getResponse(Throwable $throwable): JsonResponse;
}
