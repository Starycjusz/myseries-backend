<?php

namespace App\Service\Exception\Handler;

use App\Exception\FormException;
use App\Service\Validator\FormErrorExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Throwable;

class FormExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(string $logsDir, bool $isDebug)
    {
    }

    /**
     * {@inheritDoc}
     */
    public static function getSupportedClass(): string
    {
        return FormException::class;
    }

    /**
     * @param FormException $throwable
     *
     * @return JsonResponse
     */
    public function getResponse(Throwable $throwable): JsonResponse
    {
        $extractor = new FormErrorExtractor();
        $responseData = $extractor->getErrorsResponse($throwable->getForm());

        return new JsonResponse(
            [
                'errors' => $responseData,
            ],
            JsonResponse::HTTP_BAD_REQUEST
        );
    }
}
