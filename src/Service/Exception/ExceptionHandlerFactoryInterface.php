<?php

namespace App\Service\Exception;

use App\Service\Exception\Handler\ExceptionHandlerInterface;
use Throwable;

interface ExceptionHandlerFactoryInterface
{
    /**
     * @param Throwable $throwable
     *
     * @return ExceptionHandlerInterface
     */
    public function getHandler(Throwable $throwable): ExceptionHandlerInterface;
}
