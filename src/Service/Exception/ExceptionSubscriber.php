<?php

namespace App\Service\Exception;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @param ExceptionHandlerFactoryInterface $exceptionHandlerFactory
     */
    public function __construct(private ExceptionHandlerFactoryInterface $exceptionHandlerFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();
        $handler = $this->exceptionHandlerFactory->getHandler($throwable);

        if ($handler) {
            $event->setResponse($handler->getResponse($throwable));
        }
    }
}
