<?php

namespace App\Service\Series;

use App\Entity\Series\Series;

interface SeriesServiceInterface
{
    /**
     * @return Series[]
     */
    public function getSeries(): array;

    /**
     * @param int $id
     *
     * @return Series
     */
    public function get(int $id): Series;

    /**
     * @return Series
     */
    public function create(): Series;

    /**
     * @param int $id
     *
     * @return Series
     */
    public function update(int $id): Series;

    /**
     * @param int $id
     */
    public function remove(int $id): void;
}
