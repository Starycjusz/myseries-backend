<?php

namespace App\Service\Series;

use App\Entity\Series\Series;
use App\Form\Type\Entity\Series\SeriesType;
use App\Repository\Series\SeriesRepository;
use App\Service\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SeriesService implements SeriesServiceInterface
{
    /**
     * @param EntityManagerInterface $em
     * @param ValidatorInterface     $validator
     */
    public function __construct(private EntityManagerInterface $em, private ValidatorInterface $validator)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getSeries(): array
    {
        return $this->getRepo()->getAll();
    }

    /**
     * {@inheritDoc}
     */
    public function get(int $id): Series
    {
        $series = $this->getRepo()->get($id);
        if (!$series) {
            throw new NotFoundHttpException();
        }

        return $series;
    }

    /**
     * {@inheritDoc}
     */
    public function create(): Series
    {
        /** @var Series $series */
        $series = $this->validator->validate(SeriesType::class)->getData();

        $this->em->persist($series);
        $this->em->flush();

        return $series;
    }

    /**
     * {@inheritDoc}
     */
    public function update(int $id): Series
    {
        $series = $this->get($id);

        $this->validator->validate(SeriesType::class, $series, [], false)->getData();

        $this->em->flush();

        return $series;
    }

    /**
     * {@inheritDoc}
     */
    public function remove(int $id): void
    {
        $this->getRepo()->remove($id);
    }

    /**
     * @return SeriesRepository
     */
    private function getRepo(): SeriesRepository
    {
        return $this->em->getRepository(Series::class);
    }
}
