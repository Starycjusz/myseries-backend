<?php

namespace App\Service\Cache;

use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

interface CacheFactoryInterface
{
    /**
     * @param string $namespace
     *
     * @return TagAwareAdapterInterface
     */
    public function getTagAware(string $namespace = ''): TagAwareAdapterInterface;
}
