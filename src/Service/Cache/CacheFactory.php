<?php

namespace App\Service\Cache;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

class CacheFactory implements CacheFactoryInterface
{
    /**
     * @param string $cacheDir
     */
    public function __construct(private string $cacheDir)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getTagAware(string $namespace = ''): TagAwareAdapterInterface
    {
        $adapter = $this->getFilesystemAdapter($namespace);

        return new TagAwareAdapter($adapter, $adapter);
    }

    /**
     * @param string $namespace
     *
     * @return FilesystemAdapter
     */
    private function getFilesystemAdapter(string $namespace = ''): FilesystemAdapter
    {
        return new FilesystemAdapter($namespace, 0, $this->cacheDir);
    }
}
