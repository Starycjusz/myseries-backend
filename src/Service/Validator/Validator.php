<?php

namespace App\Service\Validator;

use App\Exception\FormException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Validator implements ValidatorInterface
{
    /**
     * @param FormFactoryInterface $formFactory
     * @param RequestStack         $requestStack
     */
    public function __construct(private FormFactoryInterface $formFactory, private RequestStack $requestStack)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $type, $data = null, array $options = [], bool $clearMissing = true): FormInterface
    {
        $form = $this->getForm($type, $data, $options);

        $this->validateFormObject($form, $clearMissing);

        return $form;
    }

    /**
     * {@inheritDoc}
     */
    public function validateFormObject(FormInterface $form, bool $clearMissing = true): Validator
    {
        $data = $this->getRequestData();

        $form->submit($data, $clearMissing);

        if ($form->isSubmitted() && !$form->isValid()) {
            throw new FormException($form);
        }

        return $this;
    }

    /**
     * @param string $type
     * @param mixed  $data
     * @param array  $options
     *
     * @return FormInterface
     */
    private function getForm(string $type, $data = null, array $options = []): FormInterface
    {
        return $this->formFactory->create($type, $data, $options);
    }

    /**
     * @return array|null
     */
    private function getRequestData(): ?array
    {
        $req = $this->requestStack->getMasterRequest();

        switch ($req->getMethod()) {
            // @codeCoverageIgnoreStart
            case 'GET':
            case 'DELETE':
                // @codeCoverageIgnoreEnd
                $data = $req->query->all();
                break;
            case 'PUT':
                $data = json_decode($req->getContent(), true);
                break;
            default:
                $data = $req->request->all();
        }

        if ($req->files->count()) {
            $data = array_merge_recursive($data, $req->files->all());
        }

        return $data;
    }
}
