<?php

namespace App\Service\Validator;

use Symfony\Component\Form\FormInterface;

/**
 * Extracts form errors from FormInterface
 */
interface FormErrorExtractorInterface
{
    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public function getErrorsResponse(FormInterface $form): array;

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public function extractFormErrors(FormInterface $form): array;
}
