<?php

namespace App\Service\Validator;

use App\Model\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolation;

class FormErrorExtractor implements FormErrorExtractorInterface
{
    const FORM_FIELD_NAME = 'form';

    /**
     * {@inheritDoc}
     */
    public function getErrorsResponse(FormInterface $form): array
    {
        $result = [];

        /** @var FormError $error */
        foreach ($this->extractFormErrors($form) as $error) {
            $result[] = [
                'field'   => $error->getField(),
                'message' => $error->getMessage(),
                'value'   => $error->getValue() ?? $error->getInvalidValue(),
            ];
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function extractFormErrors(FormInterface $form): array
    {
        $errors = $form->getErrors(true, true);
        $formName = $form->getName();
        $items = [];

        foreach ($errors as $error) {
            $fieldPath = null;
            $message = $error->getMessage();

            $parent = $error->getOrigin()->getParent();
            if ($parent && $parent->getConfig()->getOption('error_path')) {
                $fieldPath = $error->getOrigin()->getName();
            }

            $cause = $error->getCause();
            if ($cause) {
                if (!isset($fieldPath)) {
                    preg_match_all("/\[([^\]]*)\]/", $cause->getPropertyPath(), $path);
                    $fieldPath = implode('.', $path[1]) ?: null;
                }

                if ($cause instanceof ConstraintViolation) {
                    $invalidValue = $cause->getInvalidValue();
                }
            }

            $name = $fieldPath ?? $error->getOrigin()->getName();
            if ($name === $formName) {
                $name = self::FORM_FIELD_NAME;
            }

            $formError = (new FormError())
                ->setField($name)
                ->setValue($this->trimValues($error->getMessageParameters()))
                ->setInvalidValue($invalidValue ?? null);

            $values = (array) $formError->getValue();
            $count = substr_count($message, '%s');
            if ($count > 0) {
                if ($count <= count($values)) {
                    $message = vsprintf($message, $values);
                }
            } else {
                foreach ($values as $key => $value) {
                    $message = str_replace(sprintf('{%s}', $key), $value, $message);
                }
            }

            $items[] = $formError->setMessage($message);
        }

        return $items;
    }

    /**
     * @param array $values
     *
     * @return mixed
     */
    protected function trimValues(array $values): mixed
    {
        $trimmedValues = [];
        foreach ($values as $key => $value) {
            $key = trim(trim($key, '{{ '), ' }}');
            $trimmedValue = trim($value, "\"");

            if ('value' === $key) {
                return $trimmedValue;
            }

            $trimmedValues[$key] = $trimmedValue;
        }

        return $trimmedValues;
    }
}
