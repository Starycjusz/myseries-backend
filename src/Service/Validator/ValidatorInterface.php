<?php

namespace App\Service\Validator;

use App\Exception\FormException;
use Symfony\Component\Form\FormInterface;

/**
 * request data validation based on symfony forms & validator component
 */
interface ValidatorInterface
{
    /**
     * @param string $type
     * @param mixed  $data
     * @param array  $options
     * @param bool   $clearMissing
     *
     * @return FormInterface
     *
     * @throws FormException when form isn't valid
     */
    public function validate(string $type, $data = null, array $options = [], bool $clearMissing = true): FormInterface;

    /**
     * @param FormInterface $form
     * @param bool          $clearMissing
     *
     * @return Validator
     *
     * @throws FormException
     */
    public function validateFormObject(FormInterface $form, bool $clearMissing = true): Validator;
}
