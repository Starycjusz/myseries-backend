<?php

namespace App\Form\Type\Entity\Series;

use App\Entity\Series\Series;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class SeriesType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'constraints' => [
                        new Constraints\Type('string'),
                        new Constraints\NotBlank(),
                        new Constraints\Length(['max' => 255]),
                    ],
                ]
            )
            ->add(
                'season',
                IntegerType::class,
                [
                    'constraints' => [
                        new Constraints\Type('int'),
                        new Constraints\NotNull(),
                        new Constraints\Positive(),
                        new Constraints\LessThan(pow(10, 10)),
                    ],
                ]
            )
            ->add(
                'episode',
                IntegerType::class,
                [
                    'constraints' => [
                        new Constraints\Type('int'),
                        new Constraints\NotNull(),
                        new Constraints\Positive(),
                        new Constraints\LessThan(pow(10, 10)),
                    ],
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_class', Series::class);
    }
}
