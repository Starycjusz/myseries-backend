<?php

namespace App\Controller\Series;

use App\Service\Series\SeriesServiceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("series")
 */
class SeriesController
{
    use ControllerTrait;

    /**
     * @param SeriesServiceInterface $service
     */
    public function __construct(private SeriesServiceInterface $service)
    {
    }

    /**
     * @Rest\Get()
     *
     * @return View
     */
    public function getAllSeries(): View
    {
        return $this->view($this->service->getSeries());
    }

    /**
     * @Rest\Get("/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return View
     */
    public function getSeries(int $id): View
    {
        return $this->view($this->service->get($id));
    }

    /**
     * @Rest\Post()
     *
     * @return View
     */
    public function postSeries(): View
    {
        $series = $this->service->create();

        return $this->view(null, Response::HTTP_CREATED, ['Id' => $series->getId()]);
    }

    /**
     * @Rest\Put("/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return View
     */
    public function putSeries(int $id): View
    {
        $this->service->update($id);

        return $this->view(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Delete("/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return View
     */
    public function deleteSeries(int $id): View
    {
        $this->service->remove($id);

        return $this->view(null, Response::HTTP_ACCEPTED);
    }
}
