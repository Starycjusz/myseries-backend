<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\View\View;

/**
 * @Rest\Route()
 */
class IndexController
{
    use ControllerTrait;

    /**
     * @Rest\Get()
     *
     * @return View
     */
    public function ping(): View
    {
        return $this->view();
    }
}
