<?php

namespace App\Model\Form;

class FormError
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    protected string $message;

    /**
     * @var array|string|null
     */
    protected mixed $value = null;

    /**
     * @var mixed
     */
    protected mixed $invalidValue;

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     *
     * @return FormError
     */
    public function setField(string $field): FormError
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return FormError
     */
    public function setMessage(string $message): FormError
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return array|string|null
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    /**
     * @param array|string|null $value
     *
     * @return FormError
     */
    public function setValue(mixed $value): FormError
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvalidValue(): mixed
    {
        return $this->invalidValue;
    }

    /**
     * @param mixed $invalidValue
     *
     * @return FormError
     */
    public function setInvalidValue(mixed $invalidValue): FormError
    {
        $this->invalidValue = $invalidValue;

        return $this;
    }
}
