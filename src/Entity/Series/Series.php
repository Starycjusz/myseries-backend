<?php

namespace App\Entity\Series;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="series")
 * @ORM\Entity(repositoryClass="App\Repository\Series\SeriesRepository")
 *
 * @UniqueEntity(fields={"name"}, message="Name is already taken.")
 */
class Series
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`name`", type="string")
     */
    private string $name;

    /**
     * @var int
     *
     * @ORM\Column(name="season", type="integer")
     */
    private int $season;

    /**
     * @var int
     *
     * @ORM\Column(name="episode", type="integer")
     */
    private int $episode;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Series
     */
    public function setId(int $id): Series
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Series
     */
    public function setName(string $name): Series
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getSeason(): int
    {
        return $this->season;
    }

    /**
     * @param int $season
     *
     * @return Series
     */
    public function setSeason(int $season): Series
    {
        $this->season = $season;

        return $this;
    }

    /**
     * @return int
     */
    public function getEpisode(): int
    {
        return $this->episode;
    }

    /**
     * @param int $episode
     *
     * @return Series
     */
    public function setEpisode(int $episode): Series
    {
        $this->episode = $episode;

        return $this;
    }
}
