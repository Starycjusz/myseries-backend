<?php

namespace App\Exception;

use Symfony\Component\Form\FormInterface;
use Throwable;

/**
 * throw when form isn't valid
 */
class FormException extends \Exception
{
    /**
     *
     * @param FormInterface  $form
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(private FormInterface $form, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     *
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }
}
