<?php

namespace App\Serializer\Normalizer\Entity\Series;

use App\Entity\Series\Series;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SeriesNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @param Series      $series
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     */
    public function normalize($series, string $format = null, array $context = []): array
    {
        return [
            'id'      => $series->getId(),
            'name'    => $series->getName(),
            'season'  => $series->getSeason(),
            'episode' => $series->getEpisode(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof Series;
    }

    /**
     * {@inheritDoc}
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
