<?php

namespace App\Serializer\Normalizer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CollectionNormalizer implements
    NormalizerInterface,
    NormalizerAwareInterface,
    DenormalizerInterface,
    DenormalizerAwareInterface,
    CacheableSupportsMethodInterface
{
    use NormalizerAwareTrait;
    use DenormalizerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, string $format = null)
    {
        return is_object($data) && $data instanceof Collection;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, string $format = null)
    {
        return is_array($data) || $data instanceof Collection;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, string $format = null, array $context = array())
    {
        return $object->map(
            function ($item) use ($format, $context) {
                return $this->normalizer->normalize($item, $format, $context);
            }
        )->getValues();
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, string $format = null, array $context = array())
    {
        return new ArrayCollection(
            array_map(
                function ($item) use ($class, $format, $context) {
                    return $this->denormalizer->denormalize($item, $class, $format, $context);
                },
                $data
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
