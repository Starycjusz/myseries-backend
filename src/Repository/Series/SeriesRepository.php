<?php

namespace App\Repository\Series;

use App\Entity\Series\Series;
use Doctrine\ORM\EntityRepository;

class SeriesRepository extends EntityRepository
{
    /**
     * @return Series[]
     */
    public function getAll(): array
    {
        return $this->findAll();
    }

    /**
     * @param int $id
     *
     * @return Series|null
     */
    public function get(int $id): ?Series
    {
        return $this->find($id);
    }

    /**
     * @param int $id
     */
    public function remove(int $id): void
    {
        $this->createQueryBuilder('series')
            ->delete()
            ->where('series.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}
