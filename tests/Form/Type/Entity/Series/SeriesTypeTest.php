<?php

namespace App\Tests\Form\Type\Entity\Series;

use App\Form\Type\Entity\Series\SeriesType;
use App\Tests\TestCase\AbstractTypeTestCase;

class SeriesTypeTest extends AbstractTypeTestCase
{
    /**
     * {@inheritDoc}
     */
    public function getFormName(): string
    {
        return SeriesType::class;
    }

    /**
     * {@inheritDoc}
     */
    public function getTestData(): array
    {
        $create = function (array $data = []): array {
            return array_replace_recursive(
                [
                    'name'    => uniqid(),
                    'season'  => rand(1, 100),
                    'episode' => rand(1, 100),
                ],
                $data
            );
        };

        return [
            [
                [
                    'valid' => false,
                    'data'  => [],
                ],
            ],
            [
                [
                    'valid' => true,
                    'data'  => $create(),
                ],
            ],
            // name
            [
                [
                    'valid' => true,
                    'data'  => $create(['name' => uniqid()]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['name' => null]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['name' => '']),
                ],
            ],
            // season
            [
                [
                    'valid' => true,
                    'data'  => $create(['season' => rand(1, 100)]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['season' => 0]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['season' => -rand(1, 100)]),
                ],
            ],
            [
                [
                    'valid' => true,
                    'data'  => $create(['season' => pow(10, 10) - 1]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['season' => pow(10, 10)]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['season' => uniqid()]),
                ],
            ],
            // episode
            [
                [
                    'valid' => true,
                    'data'  => $create(['episode' => rand(1, 100)]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['episode' => 0]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['episode' => -rand(1, 100)]),
                ],
            ],
            [
                [
                    'valid' => true,
                    'data'  => $create(['episode' => pow(10, 10) - 1]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['episode' => pow(10, 10)]),
                ],
            ],
            [
                [
                    'valid' => false,
                    'data'  => $create(['episode' => uniqid()]),
                ],
            ],
        ];
    }
}
