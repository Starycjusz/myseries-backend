<?php

namespace App\Tests\Controller;

use App\Tests\TestCase\AbstractTestCase;
use Symfony\Component\HttpFoundation\Response;

class IndexControllerTest extends AbstractTestCase
{
    /**
     * [GET] /
     * testing ping
     */
    public function testPing()
    {
        // Act
        $result = $this->request()->getCode();

        // Assert
        $this->assertSame(Response::HTTP_NO_CONTENT, $result);
    }
}
