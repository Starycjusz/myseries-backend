<?php

namespace App\Tests\TestCase;

use Exception;
use Symfony\Component\Form\FormInterface;

abstract class AbstractTypeTestCase extends AbstractTestCase implements TypeTestCaseInterface
{
    /**
     * @var FormInterface
     */
    private static $formBuilder;

    /**
     * @var string
     */
    private static $hash;

    /**
     * @dataProvider getTestData
     *
     * @param array $data
     */
    public function testFormValidation(array $data)
    {
        // Arrange
        $model = $data['model'] ?? $this->getFormData();
        $config = $data['config'] ?? $this->getConfig();

        try {
            $hash = serialize([$this->getFormName(), $model, $config]);
        } catch (Exception $e) {
            $hash = serialize([$this->getFormName(), $model]);
        }

        if (self::$hash !== $hash) {
            self::$formBuilder = $this->getFormFactory()->createBuilder($this->getFormName(), $model, $config);
            self::$hash = $hash;
        }

        $form = self::$formBuilder->getForm();

        // Act
        $form->submit($data['data'], $data['clearMissing'] ?? $this->getClearMissing());

        // Assert
        $this->assertTrue($form->isSynchronized());
        $this->assertSame(
            $data['valid'],
            $form->isValid(),
            $form->isValid() ? '' : $form->getErrors(true, true)
        );

        if ($model) {
            $this->assertSame($model, $form->getData());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFormData(): mixed
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(): array
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getClearMissing(): bool
    {
        return true;
    }
}
