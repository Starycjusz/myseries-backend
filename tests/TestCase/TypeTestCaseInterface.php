<?php

namespace App\Tests\TestCase;

interface TypeTestCaseInterface
{
    /**
     * @return string
     */
    public function getFormName(): string;

    /**
     * @return array
     */
    public function getTestData(): array;

    /**
     * @return array
     */
    public function getConfig(): array;

    /**
     * @return mixed
     */
    public function getFormData(): mixed;
}
