<?php

namespace App\Tests\TestCase;

use App\Service\Validator\ValidatorInterface;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormFactoryBuilder;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ValidatorBuilder;

class AbstractTestCase extends WebTestCase
{
    use FixturesTrait;

    /**
     * @var KernelBrowser
     */
    private KernelBrowser $client;

    /**
     * @var ReferenceRepository
     */
    protected ReferenceRepository $fixtures;

    /**
     * @param string $method
     * @param string $uri
     * @param array  $parameters
     * @param array  $files
     * @param array  $server
     *
     * @return AbstractTestCase
     */
    protected function request(
        string $method = 'GET',
        string $uri = '/',
        array $parameters = [],
        array $files = [],
        array $server = []
    ): AbstractTestCase
    {
        if ('PUT' === $method) {
            $content = json_encode($parameters);
        }

        $this->getHttpClient()->request($method, $uri, $parameters, $files, $server, $content ?? null);

        return $this;
    }

    /**
     * @param string $method
     * @param array  $params
     * @param array  $headers
     *
     * @return Request
     */
    protected function createRequest($method = 'GET', $params = [], $headers = []): Request
    {
        $request = $method === 'PUT' ?
            Request::create('', $method, [], [], [], [], json_encode($params)) :
            Request::create('', $method, $params);

        $request->headers->add($headers);

        return $request;
    }

    /**
     * @param array $fixtures
     */
    protected function setFixtures(array $fixtures = []): void
    {
        $this->fixtures = $this->loadFixtures($fixtures)->getReferenceRepository();
    }

    /**
     * @param string $name
     *
     * @return object
     */
    protected function getReference(string $name): object
    {
        return $this->fixtures->getReference($name);
    }

    /**
     * @param string $name
     *
     * @return MockObject
     */
    protected function getServiceMock(string $name): MockObject
    {
        return $this->getMockBuilder($name)->disableOriginalConstructor()->getMock();
    }

    /**
     * @return int
     */
    protected function getCode(): int
    {
        return $this->getClientResponse()->getStatusCode();
    }

    /**
     * @return mixed
     */
    protected function getContent(): mixed
    {
        return json_decode($this->getClientResponse()->getContent());
    }

    /**
     * @return Response
     */
    protected function getClientResponse(): Response
    {
        return $this->getHttpClient()->getResponse();
    }

    /**
     * @return KernelBrowser
     */
    protected function getHttpClient(): KernelBrowser
    {
        if (!isset($this->client)) {
            $this->client = static::createClient();
        }

        return $this->client;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getManager(): EntityManagerInterface
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @param string $type
     * @param mixed  $data
     *
     * @return ValidatorInterface
     */
    protected function prepareValidator(string $type, mixed $data): ValidatorInterface
    {
        $form = $this->getServiceMock(FormInterface::class);
        $form->expects($this->atMost(1))->method('getData')->willReturn($data);

        $validator = $this->getServiceMock(ValidatorInterface::class);
        $validator->expects($this->once())->method('validate')->with($type)->willReturn($form);

        return $validator;
    }

    /**
     * @return FormFactoryInterface
     */
    final protected function getFormFactory(): FormFactoryInterface
    {
        static $factory = null;

        if (is_null($factory)) {
            $builder = new FormFactoryBuilder();

            $validatorBuilder = new ValidatorBuilder();
            $builder->addExtension(new ValidatorExtension($validatorBuilder->getValidator()));

            $factory = $builder->getFormFactory();
        }

        return $factory;
    }
}
