<?php

namespace App\Tests\TestCase;

use StdClass;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class AbstractNormalizerTestCase extends AbstractTestCase
{
    /**
     * get normalizer to tests supporting types
     *
     * @return NormalizerInterface
     */
    abstract public function getNormalizer(): NormalizerInterface;

    /**
     * get supported type instance(s)
     *
     * @return mixed
     */
    abstract public function getSupported(): mixed;

    /**
     * testing supported types
     */
    public function testSupportedTypes()
    {
        // Arrange
        $types = $this->getSupported();

        if (!is_iterable($types)) {
            $types = [$types];
        }

        $normalizer = $this->getNormalizer();

        foreach ($types as $type) {
            // Act
            $result = $normalizer->supportsNormalization($type, uniqid());

            // Assert
            $this->assertTrue($result);
        }
    }

    /**
     * testing supporting normalization types
     *
     * @dataProvider typesProvider
     *
     * @param mixed $data
     */
    public function testNotSupportedTypes(mixed $data)
    {
        // Arrange
        $normalizer = $this->getNormalizer();

        // Act
        $result = $normalizer->supportsNormalization($data, uniqid());

        // Assert
        $this->assertFalse($result);
    }

    /**
     * @return array
     */
    public function typesProvider(): array
    {
        return [
            [uniqid()],
            [new StdClass()],
            [null],
            [rand(1, 100)],
        ];
    }
}
