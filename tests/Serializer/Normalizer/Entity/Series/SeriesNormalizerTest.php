<?php

namespace App\Tests\Serializer\Normalizer\Entity\Series;

use App\Entity\Series\Series;
use App\Serializer\Normalizer\Entity\Series\SeriesNormalizer;
use App\Tests\TestCase\AbstractNormalizerTestCase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SeriesNormalizerTest extends AbstractNormalizerTestCase
{
    /**
     * {@inheritdoc}
     */
    public function getNormalizer(): NormalizerInterface
    {
        return new SeriesNormalizer();
    }

    /**
     * {@inheritdoc}
     */
    public function getSupported(): Series
    {
        return new Series();
    }

    /**
     * testing normalization
     */
    public function testNormalize()
    {
        // Arrange
        $series = (new Series())
            ->setId(rand(1, 100))
            ->setName(uniqid())
            ->setSeason(rand(1, 100))
            ->setEpisode(rand(1, 100));

        $normalizer = new SeriesNormalizer();

        // Act
        $result = $normalizer->normalize($series);

        // Assert
        $this->assertTrue(is_array($result));
        $this->assertCount(4, $result);

        $this->assertSame($series->getId(), $result['id']);
        $this->assertSame($series->getName(), $result['name']);
        $this->assertSame($series->getSeason(), $result['season']);
        $this->assertSame($series->getEpisode(), $result['episode']);
    }
}
