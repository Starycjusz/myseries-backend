<?php

namespace App\Tests\Serializer\Normalizer;

use App\Serializer\Normalizer\CollectionNormalizer;
use App\Tests\TestCase\AbstractTestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use StdClass;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CollectionNormalizerTest extends AbstractTestCase
{
    /**
     * testing supporting normalization types
     *
     * @dataProvider normalizableTypesProvider
     *
     * @param mixed $data
     * @param bool  $supportsNormalization
     */
    public function testSupportNormalization(mixed $data, bool $supportsNormalization)
    {
        // Arrange
        $normalizer = new CollectionNormalizer();

        // Act
        $result = $normalizer->supportsNormalization($data, uniqid());

        // Assert
        $this->assertSame($supportsNormalization, $result);
    }

    /**
     * testing supporting denormalization types
     *
     * @dataProvider denormalizableTypesProvider
     *
     * @param mixed $data
     * @param bool  $supportsDenormalization
     */
    public function testSupportDenormalization($data, bool $supportsDenormalization)
    {
        // Arrange
        $normalizer = new CollectionNormalizer();

        // Act
        $result = $normalizer->supportsDenormalization(
            $data,
            is_object($data) ? get_class($data) : gettype($data),
            uniqid()
        );

        // Assert
        $this->assertSame($supportsDenormalization, $result);
    }

    /**
     * testing normalization
     */
    public function testNormalize()
    {
        // Arrange
        $collectionSize = rand(10, 30);
        $collection = new ArrayCollection();
        for ($i = 0; $i < $collectionSize; $i++) {
            $collection->add(uniqid());
        }

        $expectedResult = uniqid();
        $childNormalizer = $this->getMockBuilder(NormalizerInterface::class)->getMock();
        $childNormalizer->expects($this->exactly($collectionSize))->method('normalize')->willReturn($expectedResult);

        $normalizer = new CollectionNormalizer();
        $normalizer->setNormalizer($childNormalizer);

        // Act
        $result = $normalizer->normalize($collection);

        // Assert
        $this->assertTrue(is_array($result));
        $this->assertCount($collectionSize, $result);
        foreach ($result as $item) {
            $this->assertSame($expectedResult, $item);
        }
    }

    /**
     * testing denormalization
     */
    public function testDenormalize()
    {
        // Arrange
        $collectionSize = rand(10, 30);
        $collection = [];
        for ($i = 0; $i < $collectionSize; $i++) {
            $collection[] = uniqid();
        }

        $expectedResult = uniqid();
        $childDenormalizer = $this->getMockBuilder(DenormalizerInterface::class)->getMock();
        $childDenormalizer->expects($this->exactly($collectionSize))->method('denormalize')->willReturn(
            $expectedResult
        );

        $normalizer = new CollectionNormalizer();
        $normalizer->setDenormalizer($childDenormalizer);

        // Act
        $result = $normalizer->denormalize($collection, uniqid());

        // Assert
        $this->assertTrue($result instanceof ArrayCollection);
        $this->assertCount($collectionSize, $result);
        foreach ($result as $item) {
            $this->assertSame($expectedResult, $item);
        }
    }

    /**
     * @return array
     */
    public function normalizableTypesProvider(): array
    {
        return [
            [uniqid(), false],
            [new ArrayCollection(), true],
            [$this->getMockBuilder(Collection::class)->disableOriginalConstructor()->getMock(), true],
            [new StdClass(), false],
            [null, false],
            [rand(1, 100), false],
            [[], false],
        ];
    }

    /**
     * @return array
     */
    public function denormalizableTypesProvider(): array
    {
        return [
            [uniqid(), false],
            [new ArrayCollection(), true],
            [$this->getMockBuilder(Collection::class)->disableOriginalConstructor()->getMock(), true],
            [new StdClass(), false],
            [null, false],
            [rand(1, 100), false],
            [[], true],
        ];
    }
}
