<?php

namespace App\Tests\Service\Series;

use App\Entity\Series\Series;
use App\Form\Type\Entity\Series\SeriesType;
use App\Service\Series\SeriesService;
use App\Service\Validator\ValidatorInterface;
use App\Tests\DataFixtures\Series\SeriesFixtures;
use App\Tests\TestCase\AbstractTestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SeriesServiceTest extends AbstractTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->setFixtures([SeriesFixtures::class]);
    }

    /**
     * testing getting all series entities
     */
    public function testGetAllSeries()
    {
        // Arrange
        $service = $this->getService();

        // Act
        $result = $service->getSeries();

        // Assert
        $this->assertNotEmpty($result);
        foreach ($result as $series) {
            $this->assertInstanceOf(Series::class, $series);
        }
    }

    /**
     * testing getting series entity by id
     */
    public function testGetSeries()
    {
        // Arrange
        $series = $this->getReference('series-2')->getId();

        $service = $this->getService();

        // Act
        $result = $service->get($series);

        // Assert

        $this->assertInstanceOf(Series::class, $result);
        $this->assertSame($series, $result->getId());
    }

    /**
     * testing getting series entity by non existing identifier
     */
    public function testGetSeriesFail()
    {
        // Arrange
        $series = rand(1000, 10000);

        $service = $this->getService();

        // Assert
        $this->expectException(NotFoundHttpException::class);

        // Act
        $service->get($series);
    }

    /**
     * testing creating series entity
     */
    public function testCreate()
    {
        // Arrange
        $series = SeriesFixtures::create();

        $validator = $this->prepareValidator(SeriesType::class, $series);

        $service = $this->getService($validator);

        // Act
        $result = $service->create();
        $dbResult = $this->getManager()->find(Series::class, $result->getId());

        // Assert - response
        $this->assertInstanceOf(Series::class, $result);
        $this->assertSeries($series, $result);

        // Assert - db
        $this->assertInstanceOf(Series::class, $dbResult);
        $this->assertSeries($series, $dbResult);
    }

    /**
     * testing updating series entity
     */
    public function testUpdate()
    {
        // Arrange
        $series = $this->getReference('series-2');
        $identifier = $series->getId();

        $validator = $this->prepareValidator(SeriesType::class, $series);

        $service = $this->getService($validator);

        // Act
        $result = $service->update($identifier);
        $dbResult = $this->getManager()->find(Series::class, $identifier);

        // Assert - response
        $this->assertInstanceOf(Series::class, $result);
        $this->assertSeries($series, $result);

        // Assert - db
        $this->assertInstanceOf(Series::class, $dbResult);
        $this->assertSeries($series, $dbResult);
    }

    /**
     * testing removing series entity
     */
    public function testRemove()
    {
        // Arrange
        $series = $this->getReference('series-2')->getId();

        $em = $this->getManager();

        $service = $this->getService();

        // Act
        $service->remove($series);
        $em->clear();
        $result = $em->getRepository(Series::class)->find($series);

        // Assert
        $this->assertNull($result);
    }

    /**
     * testing removing non existing series entity
     */
    public function testRemoveDoesntExists()
    {
        // Arrange
        $series = rand(1000, 1000);

        $em = $this->getManager();

        $service = $this->getService();

        // Act
        $service->remove($series);
        $em->clear();
        $result = $em->getRepository(Series::class)->find($series);

        // Assert
        $this->assertNull($result);
    }

    /**
     * @param Series $expected
     * @param Series $actual
     */
    private function assertSeries(Series $expected, Series $actual): void
    {
        $this->assertSame($expected->getName(), $actual->getName());
        $this->assertSame($expected->getSeason(), $actual->getSeason());
        $this->assertSame($expected->getEpisode(), $actual->getEpisode());
    }

    /**
     * @param ValidatorInterface|null $validator
     *
     * @return SeriesService
     */
    private function getService(ValidatorInterface $validator = null): SeriesService
    {
        return new SeriesService(
            $this->getManager(),
            $validator ?? $this->getServiceMock(ValidatorInterface::class)
        );
    }
}
