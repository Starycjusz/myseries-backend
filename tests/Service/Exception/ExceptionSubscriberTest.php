<?php

namespace App\Tests\Service\Exception;

use App\Service\Exception\Handler\ExceptionHandlerInterface;
use Exception;
use App\Service\Exception\ExceptionHandlerFactoryInterface;
use App\Service\Exception\ExceptionSubscriber;
use App\Tests\TestCase\AbstractTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelInterface;

class ExceptionSubscriberTest extends AbstractTestCase
{
    /**
     * testing handling kernel exceptions
     */
    public function testKernelException()
    {
        // Arrange
        $throwable = new Exception();
        $response = new JsonResponse();

        $event = new ExceptionEvent(
            $this->getServiceMock(KernelInterface::class),
            $this->getServiceMock(Request::class),
            0,
            $throwable
        );

        $handler = $this->getServiceMock(ExceptionHandlerInterface::class);
        $handler->expects($this->once())->method('getResponse')->with($throwable)->willReturn($response);

        $handlerFactory = $this->getServiceMock(ExceptionHandlerFactoryInterface::class);
        $handlerFactory->expects($this->once())->method('getHandler')->with($throwable)->willReturn($handler);

        $subscriber = $this->getSubscriber($handlerFactory);

        // Act
        $subscriber->onKernelException($event);
        $result = $event->getResponse();

        // Assert
        $this->assertSame($response, $result);
    }

    /**
     * @param ExceptionHandlerFactoryInterface|null $exceptionHandlerFactory
     *
     * @return ExceptionSubscriber
     */
    private function getSubscriber(
        ExceptionHandlerFactoryInterface $exceptionHandlerFactory = null
    ): ExceptionSubscriber
    {
        return new ExceptionSubscriber(
            $exceptionHandlerFactory ?? $this->getServiceMock(ExceptionHandlerFactoryInterface::class)
        );
    }
}
