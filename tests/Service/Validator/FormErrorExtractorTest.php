<?php

namespace App\Tests\Service\Validator;

use App\Model\Form\FormError as CustomFormError;
use App\Service\Validator\FormErrorExtractor;
use App\Tests\Form\Type\FooTestType;
use App\Tests\TestCase\AbstractTestCase;
use Symfony\Component\Form\FormError;

class FormErrorExtractorTest extends AbstractTestCase
{
    /**
     * testing extracting errors array from form object
     */
    public function testExtractFormErrors()
    {
        // Arrange            
        $formFactory = $this->getFormFactory();

        $form = $formFactory->create(FooTestType::class, null, []);
        $form->submit(
            [
                'username' => 'test',
            ]
        );

        $extractor = new FormErrorExtractor();

        // Act
        $result = $extractor->extractFormErrors($form);

        // Assert
        $this->assertTrue(is_array($result));
        $this->assertInstanceOf(CustomFormError::class, $result[0]);
        $this->assertEquals('This value should not be blank.', $result[0]->getMessage());
    }

    /**
     * testing getting errors response
     */
    public function testGetErrorResponse()
    {
        // Arrange                
        $formFactory = $this->getFormFactory();

        $errorValues = [uniqid(), uniqid()];

        $form = $formFactory->create(FooTestType::class, null, []);
        $form->submit(
            [
                'username' => 'test',
            ]
        );
        $form->addError(
            new FormError(
                '123'
                , null,
                [
                    'value'  => $errorValues[0],
                    'value2' => $errorValues[1],
                ]
            )
        );

        $expectedResult = [
            [
                'field'   => 'form',
                'message' => '123',
                'value'   => $errorValues[0],
            ],
            [
                'field'   => 'password',
                'message' => 'This value should not be blank.',
                'value'   => 'null',
            ],
        ];

        $extractor = new FormErrorExtractor();

        // Act
        $result = $extractor->getErrorsResponse($form);

        // Assert
        $this->assertSame($expectedResult, $result);
    }
}
