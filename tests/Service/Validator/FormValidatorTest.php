<?php

namespace App\Tests\Service\Validator;

use App\Exception\FormException;
use App\Service\Validator\Validator;
use App\Tests\Form\Type\FooTestType;
use App\Tests\TestCase\AbstractTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class FormValidatorTest extends AbstractTestCase
{
    /**
     * testing valid form
     */
    public function testValidForm()
    {
        // Arrange
        $request = Request::create(
            '',
            'POST',
            [
                'username' => uniqid(),
                'password' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $form = $validator->validate(FooTestType::class);

        // Assert
        $this->assertTrue($form->isValid());
    }

    /**
     * testing invalid form
     */
    public function testInvalidForm()
    {
        // Assert
        $this->expectException(FormException::class);

        // Arrange
        $request = Request::create(
            '',
            'POST',
            [
                'username' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $validator->validate(FooTestType::class);
    }

    /**
     * validating form from GET request
     */
    public function testValidGetForm()
    {
        // Arrange
        $request = Request::create(
            '',
            'GET',
            [
                'username' => uniqid(),
                'password' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $form = $validator->validate(FooTestType::class);

        // Assert
        $this->assertTrue($form->isValid());
    }

    /**
     * testing invalid form from GET request
     */
    public function testInvalidGetForm()
    {
        // Assert
        $this->expectException(FormException::class);

        // Arrange
        $request = Request::create(
            '',
            'GET',
            [
                'username' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $validator->validate(FooTestType::class);
    }

    /**
     * validating form from DELETE request
     */
    public function testValidDeleteForm()
    {
        // Arrange
        $request = Request::create('', 'DELETE');
        $request->query->add(
            [
                'username' => uniqid(),
                'password' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $form = $validator->validate(FooTestType::class);

        // Assert
        $this->assertTrue($form->isValid());
    }

    /**
     * testing invalid form from DELETE request
     */
    public function testInvalidDeleteForm()
    {
        // Assert
        $this->expectException(FormException::class);

        // Arrange
        $request = Request::create(
            '',
            'DELETE',
            [
                'username' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $validator->validate(FooTestType::class);
    }

    /**
     * validating form from PUT request
     */
    public function testValidPutForm()
    {
        // Arrange
        $request = Request::create(
            '',
            'PUT',
            [],
            [],
            [],
            [],
            json_encode(
                [
                    'username' => uniqid(),
                    'password' => uniqid(),
                ]
            )
        );

        $validator = $this->getValidator($request);

        // Act
        $form = $validator->validate(FooTestType::class);

        // Assert
        $this->assertTrue($form->isValid());
    }

    /**
     * testing invalid form from PUT request
     */
    public function testInvalidPutForm()
    {
        // Assert
        $this->expectException(FormException::class);

        // Arrange
        $request = Request::create(
            '',
            'PUT',
            [
                'username' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $validator->validate(FooTestType::class);
    }

    /**
     * testing adding error to validated form and throwing exception
     */
    public function testAddErrorAndThrowException()
    {
        // Arrange
        $request = Request::create(
            '',
            'POST',
            [
                'username' => uniqid(),
                'password' => uniqid(),
            ]
        );

        $validator = $this->getValidator($request);

        // Act
        $form = $validator->validate(FooTestType::class);

        // Assert
        $this->assertTrue($form->isValid());
    }

    /**
     * @param Request|null $request
     *
     * @return Validator
     */
    private function getValidator(Request $request = null): Validator
    {
        $requestStack = new RequestStack();
        if ($request) {
            $requestStack->push($request);
        }

        return new Validator(
            $this->getFormFactory(),
            $requestStack
        );
    }
}
