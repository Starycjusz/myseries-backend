<?php

namespace App\Tests\Service\Cache;

use App\Service\Cache\CacheFactory;
use App\Tests\TestCase\AbstractTestCase;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;

class CacheFactoryTest extends AbstractTestCase
{
    /**
     * testing getting tag aware adapter
     */
    public function testGetTagAwareAdapter()
    {
        // Arrange
        $cacheFactory = $this->getCacheFactory();

        // Act
        $result = $cacheFactory->getTagAware();

        // Assert
        $this->assertInstanceOf(TagAwareAdapter::class, $result);
    }

    /**
     * @return CacheFactory
     */
    private function getCacheFactory(): CacheFactory
    {
        return new CacheFactory(
            __DIR__
        );
    }
}
