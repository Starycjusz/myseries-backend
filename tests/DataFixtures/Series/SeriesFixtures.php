<?php

namespace App\Tests\DataFixtures\Series;

use App\Entity\Series\Series;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

class SeriesFixtures extends AbstractFixture
{
    /**
     * @return Series
     */
    public static function create(): Series
    {
        return (new Series())
            ->setName(uniqid())
            ->setSeason(rand(1, 100))
            ->setEpisode(rand(1, 100));
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $series1 = self::create();
        $series2 = self::create();
        $series3 = self::create();

        $manager->persist($series1);
        $manager->persist($series2);
        $manager->persist($series3);

        $manager->flush();

        $this->setReference('series-1', $series1);
        $this->setReference('series-2', $series2);
        $this->setReference('series-3', $series3);
    }
}
